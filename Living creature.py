class LivingCreature:
    def __init__(self, birth_year, is_living):
        self.is_living = is_living
        self.birth_year = birth_year

    def __bool__(self):
        return self.is_living > 0

    def __eq__(self, other):
        return self.birth_year == other.birth_year

    def __lt__(self, other):
        return self.birth_year < other.birth_year

    def __gt__(self, other):
        return self.birth_year > other.birth_year

    def __le__(self, other):
        return self.birth_year <= other.birth_year

    def __ge__(self, other):
        return self.birth_year >= other.birth_year

    def __ne__(self, other):
        return self.birth_year != other.birth_year


a = LivingCreature(2000, 1)
b = LivingCreature(2001, 1)


print("1. A is the same age as B = {}".format(a == b))
print("2. A is younger than B = {}".format(a < b))
print("3. A is older than B = {}".format(a > b))
print("4. A is the same age or younger than B = {}".format(a <= b))
print("5. A is the same age or older than B = {}".format(a >= b))
print("6. A is not the same age as B = {}".format(a != b))
print("7. A is living".format(a.is_living))
print("8. B is living".format(b.is_living))


class Flower(LivingCreature):
    def __init__(self, petals):
        self.petals = petals

    def __eq__(self, other):
        return self.petals == other.petals

    def __lt__(self, other):
        return self.petals < other.petals

    def __gt__(self, other):
        return self.petals > other.petals

    def __le__(self, other):
        return self.petals <= other.petals

    def __ge__(self, other):
        return self.petals >= other.petals

    def __ne__(self, other):
        return self.petals != other.petals


a = Flower(20)
b = Flower(7)


print("1. A has the same amount of petals as B = {}".format(a == b))
print("2. A has less petals than B = {}".format(a < b))
print("3. A has more petals than B = {}".format(a > b))
print("4. A has the same amount of petals or less than B = {}".format(a <= b))
print("5. A has the same amount of petals or more than B = {}".format(a >= b))
print("6. A has different amount of petals as B = {}".format(a != b))


class Fish(LivingCreature):
    def __init__(self, speed):
        self.speed = speed

    def __eq__(self, other):
        return self.speed == other.speed

    def __lt__(self, other):
        return self.speed < other.speed

    def __gt__(self, other):
        return self.speed > other.speed

    def __le__(self, other):
        return self.speed <= other.speed

    def __ge__(self, other):
        return self.speed >= other.speed

    def __ne__(self, other):
        return self.speed != other.speed


a = Fish(2)
b = Fish(3)

print("1. A is as fast as B = {}".format(a == b))
print("2. A is slower than B = {}".format(a < b))
print("3. A is faster than B = {}".format(a > b))
print("4. A is as fast as or slower than B = {}".format(a <= b))
print("5. A is as fast as or faster than B = {}".format(a >= b))
print("6. A is not as fast as B = {}".format(a != b))


class Mammal(LivingCreature):
    def __init__(self, height):
        self.height = height

    def __eq__(self, other):
        return self.height == other.height

    def __lt__(self, other):
        return self.height < other.height

    def __gt__(self, other):
        return self.height > other.height

    def __le__(self, other):
        return self.height <= other.height

    def __ge__(self, other):
        return self.height >= other.height

    def __ne__(self, other):
        return self.height != other.height


a = Mammal(1.75)
b = Mammal(4)


print("1. A is the same height as B = {}".format(a == b))
print("2. A is shorter than B = {}".format(a < b))
print("3. A is taller than B = {}".format(a > b))
print("4. A is the same height or shorter than B = {}".format(a <= b))
print("5. A is the same height or taller than B = {}".format(a >= b))
print("6. A is not the same height as B = {}".format(a != b))


class Shark(Fish):
    def __init__(self, teeth):
        self.teeth = teeth

    def __eq__(self, other):
        return self.teeth == other.teeth

    def __lt__(self, other):
        return self.teeth < other.teeth

    def __gt__(self, other):
        return self.teeth > other.teeth

    def __le__(self, other):
        return self.teeth <= other.teeth

    def __ge__(self, other):
        return self.teeth >= other.teeth

    def __ne__(self, other):
        return self.teeth != other.teeth


a = Shark(50)
b = Shark(46)


print("1. A has the same amount of teeth as B = {}".format(a == b))
print("2. A has less teeth than B = {}".format(a < b))
print("3. A has more teeth than B = {}".format(a > b))
print("4. A has the same amount of teeth or less than B = {}".format(a <= b))
print("5. A has the same amount of teeth or more than B = {}".format(a >= b))
print("6. A has different amount of teeth as B = {}".format(a != b))


class Human(Mammal):
    def __init__(self, iq):
        self.iq = iq

    def __eq__(self, other):
        return self.iq == other.iq

    def __lt__(self, other):
        return self.iq < other.iq

    def __gt__(self, other):
        return self.iq > other.iq

    def __le__(self, other):
        return self.iq <= other.iq

    def __ge__(self, other):
        return self.iq >= other.iq

    def __ne__(self, other):
        return self.iq != other.iq


a = Human(120)
b = Human(110)


print("1. A is as smart as B = {}".format(a == b))
print("2. A is less smart than B = {}".format(a < b))
print("3. A is smarter than B = {}".format(a > b))
print("4. A is as smart as or less smart than B = {}".format(a <= b))
print("5. A is as smart as or smarter than B = {}".format(a >= b))
print("6. A is not as smart as B = {}".format(a != b))
