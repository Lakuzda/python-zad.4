import sys


def get_name():
    name = input("Put your name here: ")
    if name.isalpha():
        my_name = "Norbert"
        if name == my_name:
            print("Hi {}! We have the same names!".format(name))
        else:
            print("Hello {}! Nice to meet you!".format(name))
    else:
        print("It can't be a name!", file=sys.stderr)
        sys.exit(-1)


get_name()
