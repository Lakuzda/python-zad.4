import sys


def get_number():
    number = input("Put your number here: ")
    if number.isnumeric():
        number = int(number)
        if number:
            print("{}".format(number))
        else:
            print("Your number is 0")
    else:
        print("It is not a number!", file=sys.stderr)
        sys.exit(-1)


get_number()
