class Square:
    def __init__(self, side_length, name):
        self.surface_area = side_length * side_length
        self.name = name

    def __eq__(self, other):
        print("Comparision type: {} == {}".format(self.name, other.name))
        return self.surface_area == other.surface_area

    def __lt__(self, other):
        print("Comparision type: {} < {}".format(self.name, other.name))
        return self.surface_area < other.surface_area

    def __gt__(self, other):
        print("Comparision type: {} > {}".format(self.name, other.name))
        return self.surface_area > other.surface_area

    def __le__(self, other):
        print("Comparision type: {} <= {}".format(self.name, other.name))
        return self.surface_area <= other.surface_area

    def __ge__(self, other):
        print("Comparision type: {} >= {}".format(self.name, other.name))
        return self.surface_area >= other.surface_area

    def __ne__(self, other):
        print("Comparision type: {} != {}".format(self.name, other.name))
        return self.surface_area != other.surface_area


a = Square(4, "A")
b = Square(5, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))
